package com.example.apiclasswork.util

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.apiclasswork.databinding.ShibeCardBinding
import com.example.apiclasswork.loadImage

class ShibeAdapter: RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>() {
    private lateinit var imgUrlList: List<String>

    class ShibeViewHolder(private val binding: ShibeCardBinding): RecyclerView.ViewHolder(binding.root) {
        fun addShibe(imgUrl: String) {
            binding.tvMain.loadImage(imgUrl)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShibeViewHolder {
        val binding = ShibeCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ShibeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        return holder.addShibe(imgUrlList[position])
    }

    override fun getItemCount(): Int {
        return imgUrlList.size
    }

    fun setImageList(images: List<String>) {
        imgUrlList = images
    }
}