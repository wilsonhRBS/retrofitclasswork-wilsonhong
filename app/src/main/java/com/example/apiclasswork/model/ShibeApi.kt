package com.example.apiclasswork.model

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ShibeApi {
    @GET("/api/shibes")
    suspend fun getShibeList(@Query("count") count: Int): Response<List<String>>

    companion object {
        val shibesListApi by lazy {
            Retrofit
                .Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://shibe.online")
                .build()
                .create(ShibeApi::class.java)
        }
    }
}