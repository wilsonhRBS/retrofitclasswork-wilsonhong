package com.example.apiclasswork.util

sealed class Resource() {
    data class Success(val data: List<String>): Resource()
    object Loading: Resource()
    data class Error(val error: String): Resource()
}
