package com.example.apiclasswork.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.apiclasswork.databinding.DisplayShibesFragmentBinding
import com.example.apiclasswork.num_grid_columns
import com.example.apiclasswork.util.Resource
import com.example.apiclasswork.util.ShibeAdapter
import com.example.apiclasswork.viewmodel.BlankViewModelFactory
import com.example.apiclasswork.viewmodel.ShibeViewModel
import com.google.android.material.snackbar.Snackbar

class DisplayShibesFragment: Fragment() {
    private var _binding: DisplayShibesFragmentBinding? = null
    private val binding: DisplayShibesFragmentBinding get() = _binding!!
    private lateinit var viewModel: ShibeViewModel

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = DisplayShibesFragmentBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, BlankViewModelFactory())[ShibeViewModel::class.java]
        initViews()
    }

    private fun initViews() = with(binding) {
        viewModel.state.observe(viewLifecycleOwner) {
            when(it) {
                is Resource.Loading -> {
                    progress.isVisible = true
                    btn1.isEnabled = false
                    btn2.isEnabled = false
                    viewModel.fetchShibe()
                }
                is Resource.Error -> {
                    Snackbar.make(requireContext(), binding.root, it.error, Snackbar.LENGTH_SHORT).show()
                    //viewModel.setLoading()
                }
                is Resource.Success -> {
                    progress.isVisible = false
                    btn1.isEnabled = true
                    btn2.isEnabled = true
                    val adapter = ShibeAdapter()
                    binding.rvShibes.layoutManager = GridLayoutManager(requireContext(), num_grid_columns)
                    adapter.setImageList(it.data)
                    binding.rvShibes.adapter = adapter
                }
            }
        }

        btn1.setOnClickListener {
            viewModel.setLoading()
        }
        btn2.setOnClickListener {
            viewModel.setLoading()
        }
    }
}