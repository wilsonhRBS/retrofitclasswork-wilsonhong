package com.example.apiclasswork

import android.widget.ImageView
import com.bumptech.glide.Glide

const val num_grid_columns = 2
const val default_count = 20

fun ImageView.loadImage(url: String) {
    Glide.with(context).load(url).into(this)
}