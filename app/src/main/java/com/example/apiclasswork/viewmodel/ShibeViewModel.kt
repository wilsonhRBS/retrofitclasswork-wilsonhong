package com.example.apiclasswork.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.apiclasswork.model.ShibeRepo
import com.example.apiclasswork.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ShibeViewModel: ViewModel() {
    private val repo = ShibeRepo

    private var _state: MutableLiveData<Resource> = MutableLiveData(Resource.Loading)
    val state: LiveData<Resource> get() = _state

    fun setLoading() {
        _state.value = Resource.Loading
    }

    fun fetchShibe() = viewModelScope.launch(Dispatchers.Main) {
        _state.value = repo.fetchShibeList()
    }
}