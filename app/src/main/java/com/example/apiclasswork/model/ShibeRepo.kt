package com.example.apiclasswork.model

import android.util.Log
import com.example.apiclasswork.default_count
import com.example.apiclasswork.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

object ShibeRepo {

    private val shibeApi by lazy { ShibeApi.shibesListApi }

    suspend fun fetchShibeList() = withContext(Dispatchers.IO) {
        return@withContext try {
                val result = shibeApi.getShibeList(default_count)
                Log.d("REPO", result.body().toString())
                if (result.isSuccessful && result.body()!!.isNotEmpty()) {
                    Resource.Success(result.body()!!)
                } else {
                    Resource.Error("RepoError1")
                }
            } catch (e: Exception) {
                Resource.Error(e.toString())
            }
    }
}